import React from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Container,
  Typography,
  Stack,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Badge,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import MailIcon from "@mui/icons-material/Mail";

import {
  ColorChip,
  getColorStatus,
  getTitleStatus,
} from "../components/ColorChip";
import { ProfileRow } from "../components/ProfileRow";
import { useGetApartments } from "../api";
import { PositionedMenu } from "../components/PosMenu";

export const Main = () => {
  const { data } = useGetApartments();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <Container maxWidth={false} sx={{ paddingTop: 3 }}>
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={2}
        marginBottom={2}
      >
        <Typography variant="h4" component="h4">
          Мои объекты
        </Typography>
        <Link to="/create">
          <Button variant="contained" color="secondary" startIcon={<AddIcon />}>
            Объект
          </Button>
        </Link>
      </Stack>

      <Paper sx={{ width: "100%" }}>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <Typography variant="caption" sx={{ color: "gray" }}>
                    Название, адрес
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="caption" sx={{ color: "gray" }}>
                    Статус
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="caption" sx={{ color: "gray" }}>
                    Предстоящие бронирования
                  </Typography>
                </TableCell>
                <TableCell />
                <TableCell />
              </TableRow>
            </TableHead>

            <TableBody>
              {data?.map((item) => (
                <TableRow>
                  <TableCell>
                    <ProfileRow
                      avatar={item?.image}
                      address={item.address}
                      name={item.name}
                    />
                  </TableCell>
                  <TableCell>
                    <ColorChip
                      label={getTitleStatus(item.status)}
                      color={getColorStatus(item.status)}
                    />
                  </TableCell>
                  <TableCell>{item.booking || "-"}</TableCell>

                  <TableCell>
                    <Badge badgeContent={item.messages} color="success">
                      <MailIcon color="action" />
                    </Badge>
                  </TableCell>

                  <TableCell>
                    <PositionedMenu id={item.id} />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={data?.length ?? 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Container>
  );
};
