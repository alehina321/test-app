import { Backdrop, CircularProgress } from "@mui/material";
import React, { useEffect } from "react";
import { useCreateApartment } from "../api";
import { useNavigate } from "react-router-dom";

export const Create = () => {
  const navigate = useNavigate();
  const { data } = useCreateApartment();
  useEffect(() => {
    if (data) navigate(`/${data?.at(-1)?.id}`);
  }, [data]);
  return (
    <Backdrop
      sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={true}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};
