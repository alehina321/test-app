import { Container } from "@mui/material";
import { useParams } from "react-router-dom";
import { useGetApartment } from "../api";
import { BreadcrumbLocation } from "../components/Breadcrumbs";
import { EditForm } from "../components/EditForm";

export const Edit = () => {
  const params = useParams();

  const { data } = useGetApartment({ id: params.id ?? "" });

  return (
    <Container maxWidth={false} sx={{ paddingTop: 3 }}>
      <BreadcrumbLocation />
      {data && <EditForm id={params.id ?? ""} initialValues={data} />}
    </Container>
  );
};
