import React from "react";
import { Routes, Route } from "react-router-dom";
import { Layout } from "./components/Layout";
import { Main } from "./pages/Main";
import { Create } from "./pages/Create";
import { Edit } from "./pages/Edit";
import { ThemeProvider, createTheme, palettes } from "@elonkit/react";
import { useState } from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

/// <reference types="@elonkit/react/lib/overrides" />

const theme = createTheme({
  palette: {
    ...palettes.common,
    ...palettes.light,
  },
});

function App() {
  const [queryClient] = useState(() => new QueryClient());
  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={theme}>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Main />} />
            <Route path="create" element={<Create />} />
            <Route path=":id" element={<Edit />} />
          </Route>
        </Routes>
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default App;
