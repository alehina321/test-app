type ApartmentStatus = "published" | "draft" | "checking" | "failed";

type Apartment = {
  name: string;
  image?: string;
  address: string;
  status: ApartmentStatus;
  booking: number;
  messages: number;
  id: string;
};
