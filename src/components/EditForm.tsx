import React, { useEffect, useMemo } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import {
  Box,
  LinearProgress,
  Stack,
  TextField,
  Typography,
  Button,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useEditApartment } from "../api";
import { VerticalStepper, steps } from "../components/Stepper";

const schema = yup.object().shape({
  name: yup.string().min(1).required("This field is required"),
  address: yup.string().min(1).required("This field is required"),
  square: yup.string().min(1).max(10).required("This field is required"),
  amount: yup.number().min(1).required("This field is required"),
  bathrooms: yup.number().min(1).required("This field is required"),
  bedrooms: yup.number().min(1).required("This field is required"),
  bed: yup.string().required("This field is required"),
});

export const EditForm = ({
  id,
  initialValues,
}: {
  id: string;
  initialValues: Apartment;
}) => {
  const navigate = useNavigate();

  const { mutateAsync } = useEditApartment({ id });

  const [activeStep, setActiveStep] = React.useState(0);

  useEffect(() => {
    steps.map((step, index) => {
      if (index < activeStep) {
        step.fields.map((field) => {
          formik.setFieldTouched(field);
          return field;
        });
      }
      return step;
    });
  }, [activeStep]);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const formik = useFormik({
    initialValues: {
      square: "",
      amount: "",
      bed: "",
      bedrooms: "",
      name: initialValues?.name,
      address: initialValues?.address,
    },
    validationSchema: schema,
    onSubmit: (values) => {
      mutateAsync({
        name: values.name ?? "",
        address: values.address ?? "",
        id,
        status: "checking",
        booking: initialValues?.booking ?? 0,
        messages: initialValues?.messages ?? 0,
      });
      navigate("/");
    },
  });

  const validDraft = useMemo(() => {
    const errValues = Object.values(formik.errors);
    return (
      errValues.find((value) => value !== "This field is required") ===
      undefined
    );
  }, [formik.errors]);

  return (
    <form onSubmit={formik.handleSubmit}>
      <Stack direction="row" spacing={2} marginTop={2}>
        <Box
          sx={{
            marginTop: "10px",
            padding: "20px",
            border: "1px solid #D3D3D3",
            borderRadius: "20px",
            width: "65%",
          }}
        >
          {activeStep === 0 && (
            <Stack spacing={2} sx={StepStyle}>
              <Typography variant="h4" component="h4">
                Основная информация
              </Typography>

              <TextField
                label="Название"
                variant="outlined"
                id="name"
                required
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />

              <TextField
                label="Адрес"
                variant="outlined"
                id="address"
                required
                value={formik.values.address}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.address && Boolean(formik.errors.address)}
                helperText={formik.touched.address && formik.errors.address}
              />
            </Stack>
          )}
          {activeStep === 1 && (
            <Stack spacing={2} sx={StepStyle}>
              <Typography variant="h4" component="h4">
                Площадь объекта
              </Typography>

              <TextField
                label="Площадь объекта"
                variant="outlined"
                id="square"
                required
                value={formik.values.square}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.square && Boolean(formik.errors.square)}
                helperText={formik.touched.square && formik.errors.square}
              />

              <TextField
                label="Количество комнат"
                variant="outlined"
                id="amount"
                required
                value={formik.values.amount}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.amount && Boolean(formik.errors.amount)}
                helperText={formik.touched.amount && formik.errors.amount}
              />
            </Stack>
          )}
          {activeStep === 2 && (
            <Stack spacing={2} sx={{ maxWidth: "600px", marginBottom: "30px" }}>
              <Typography variant="h4" component="h4">
                Спальные места
              </Typography>

              <TextField
                type="number"
                label="Количество спален"
                variant="outlined"
                id="bedrooms"
                required
                value={formik.values.bedrooms}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.bedrooms && Boolean(formik.errors.bedrooms)
                }
                helperText={formik.touched.bedrooms && formik.errors.bedrooms}
              />

              <TextField
                label="Тип кровати"
                variant="outlined"
                id="bed"
                required
                value={formik.values.bed}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.bed && Boolean(formik.errors.bed)}
                helperText={formik.touched.bed && formik.errors.bed}
              />
            </Stack>
          )}

          <Stack spacing={2}>
            <LinearProgress
              variant="determinate"
              color="secondary"
              value={33 * (activeStep + 1)}
            />
            <Stack direction="row" justifyContent="space-between" spacing={2}>
              <Button
                variant="outlined"
                color="inherit"
                onClick={activeStep > 0 ? handleBack : () => navigate("/")}
              >
                Назад
              </Button>

              <Stack direction="row" spacing={2}>
                <Button
                  variant="contained"
                  color="inherit"
                  disabled={!validDraft}
                  onClick={() => {
                    mutateAsync({
                      name: formik.values.name ?? "",
                      address: formik.values.address ?? "",
                      id,
                      status: "draft",
                      booking: initialValues?.booking ?? 0,
                      messages: initialValues?.messages ?? 0,
                    });
                    navigate("/");
                  }}
                >
                  Сохранить черновик
                </Button>
                {activeStep === 2 ? (
                  <Button
                    variant="contained"
                    type="submit"
                    disabled={!formik.isValid}
                  >
                    Сохранить
                  </Button>
                ) : (
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={handleNext}
                  >
                    Далее
                  </Button>
                )}
              </Stack>
            </Stack>
          </Stack>
        </Box>
        <Box
          sx={{
            marginTop: "10px",
            padding: "20px",
            border: "1px solid #D3D3D3",
            borderRadius: "20px",
            width: "30%",
          }}
        >
          <VerticalStepper
            activeStep={activeStep}
            onSelect={setActiveStep}
            errors={formik.errors}
          />
        </Box>
      </Stack>
    </form>
  );
};

const StepStyle = { maxWidth: "600px", marginBottom: "30px" };
