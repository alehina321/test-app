import { Breadcrumb, Breadcrumbs } from "@elonkit/react";
import { useLocation, useNavigate } from "react-router-dom";

const BREADCRUMBS = [
  { name: "Мои объекты", path: "/" },
  { name: "Создание оъекта" },
];

export const BreadcrumbLocation = () => {
  const location = useLocation();
  const navigate = useNavigate();

  return (
    <Breadcrumbs>
      {BREADCRUMBS.map((item) => (
        <Breadcrumb
          key={item.name}
          component="button"
          onClick={() => item.path && navigate(item.path)}
        >
          {item.name}
        </Breadcrumb>
      ))}
    </Breadcrumbs>
  );
};
