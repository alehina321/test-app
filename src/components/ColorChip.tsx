import React from "react";
import { Chip, createTheme, ThemeProvider } from "@mui/material";

type ColorType =
  | "default"
  | "success"
  | "info"
  | "error"
  | "secondary"
  | "primary"
  | "warning";

export const getColorStatus = (status: ApartmentStatus): ColorType => {
  const variants: {
    [key: string]: ColorType;
  } = {
    published: "success",
    draft: "default",
    checking: "info",
    failed: "error",
  };
  return variants[status];
};

type TitleType =
  | "Опубликовано"
  | "Черновик"
  | "На модерации"
  | "Не прошло модерацию";

export const getTitleStatus = (status: ApartmentStatus): TitleType => {
  const variants: {
    [key: string]: TitleType;
  } = {
    published: "Опубликовано",
    draft: "Черновик",
    checking: "На модерации",
    failed: "Не прошло модерацию",
  };
  return variants[status];
};

const theme = createTheme({
  palette: {
    info: { main: "#90caf9" },
    success: { main: "#81c784" },
    error: { main: "#e57373" },
  },
});

export const ColorChip = ({ ...props }: React.ComponentProps<typeof Chip>) => (
  <ThemeProvider theme={theme}>
    <Chip sx={{ borderRadius: "5px", color: "black" }} {...props} />
  </ThemeProvider>
);
