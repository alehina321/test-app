import React, { Fragment, useState } from "react";
import {
  Sidenav,
  SidenavContext,
  SidenavItem,
  SidebarToggle,
  SidebarSpacer,
  SidebarScrollable,
  SidebarMenu,
  SidebarItem,
  SidebarDivider,
  Sidebar,
} from "@elonkit/react";
import HouseIcon from "@mui/icons-material/House";
import MailIcon from "@mui/icons-material/Mail";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import NotificationsIcon from "@mui/icons-material/Notifications";
import { Typography, Avatar } from "@mui/material";

import { useNavigate, useLocation, useParams } from "react-router-dom";
import { Badge } from "@mui/material";

const SidebarHeading = (props: { title: string }) => (
  <>
    <Typography sx={{ padding: "16px", color: "monoA.A900" }} variant="h6">
      {props.title}
    </Typography>
    <SidebarDivider />
  </>
);

const MIN_WIDTH = 220;
const MAX_WIDTH = 300;
const WIDTH = 287;

export const SideNav = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const params = useParams();
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Sidenav open={isOpen} onClose={() => setIsOpen(false)}>
      <Sidebar>
        <SidebarMenu>
          <SidenavItem
            icon={<HouseIcon />}
            selected={location.pathname === "/"}
            onClick={() => navigate("/")}
          />
        </SidebarMenu>

        <SidebarToggle open={isOpen} onClick={() => setIsOpen(!isOpen)} />

        <SidebarMenu>
          <SidenavItem
            icon={<PersonAddIcon />}
            id="2"
            text={"Арендадатель"}
            onClick={() => {
              navigate("/");
            }}
          />
        </SidebarMenu>

        <SidebarSpacer />

        <SidebarMenu>
          <SidenavItem
            icon={
              <Badge badgeContent={0} color="success">
                <MailIcon />
              </Badge>
            }
          />
        </SidebarMenu>

        <SidebarMenu>
          <SidenavItem
            icon={
              <Badge badgeContent={3} color="success">
                <NotificationsIcon />
              </Badge>
            }
          />
        </SidebarMenu>

        <SidebarDivider />

        <SidebarMenu>
          <SidenavItem
            icon={
              <Avatar
                alt="Remy Sharp"
                src="/static/images/avatar/1.jpg"
                variant="rounded"
              />
            }
            text="Профиль"
          />
        </SidebarMenu>
      </Sidebar>

      <Sidebar maxWidth={MAX_WIDTH} minWidth={MIN_WIDTH} width={WIDTH}>
        <SidenavContext.Consumer>
          {(value) => {
            switch (value?.itemId) {
              default:
              case "1":
                return (
                  <Fragment key="1">
                    <SidebarHeading title={"Проекты"} />

                    <SidebarScrollable>
                      <SidebarMenu>
                        <SidebarItem
                          icon={<HouseIcon />}
                          text={"Все проекты"}
                        />
                        <SidebarItem
                          icon={<HouseIcon />}
                          id="1"
                          text={"Документы"}
                        />

                        <SidebarItem
                          icon={<HouseIcon />}
                          id="2"
                          text={"Новые проекты"}
                        />
                      </SidebarMenu>
                    </SidebarScrollable>
                  </Fragment>
                );
              case "2":
                return (
                  <Fragment key="2">
                    <SidebarHeading title={"Арендадатель"} />

                    <SidebarScrollable>
                      <SidebarMenu>
                        <SidebarItem
                          icon={<HouseIcon />}
                          id="1"
                          text={"Мои объекты"}
                        />
                        <SidebarItem
                          icon={<HouseIcon />}
                          id="2"
                          text={"Календарь"}
                        />

                        <SidebarItem
                          icon={<HouseIcon />}
                          id="3"
                          text={"Бронирования"}
                        />
                      </SidebarMenu>
                    </SidebarScrollable>
                  </Fragment>
                );
            }
          }}
        </SidenavContext.Consumer>
      </Sidebar>
    </Sidenav>
  );
};
