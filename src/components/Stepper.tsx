import {
  createTheme,
  Step,
  StepLabel,
  Stepper,
  ThemeProvider,
} from "@mui/material";
import { FormikErrors } from "formik";

type FieldType =
  | "square"
  | "amount"
  | "bathrooms"
  | "bedrooms"
  | "name"
  | "address";

type StepType = { label: string; fields: FieldType[] };

export const steps: StepType[] = [
  { label: "Основная информация", fields: ["name", "address"] },
  { label: "Площадь объекта", fields: ["square", "amount"] },
  { label: "Спальные места", fields: ["bedrooms", "bathrooms"] },
];

const theme = createTheme({
  palette: {
    primary: { main: "#357a38" },
  },
});

type StepperType = {
  activeStep: number;
  errors: FormikErrors<{
    square: string;
    amount: string;
    bathrooms: string;
    bedrooms: string;
    name: string;
    address: string;
  }>;
  onSelect: (elem: number) => void;
};

export const VerticalStepper = ({
  activeStep,
  errors,
  onSelect,
}: StepperType) => (
  <ThemeProvider theme={theme}>
    <Stepper activeStep={activeStep} orientation="vertical">
      {steps.map((step, index) => {
        return (
          <Step
            key={index}
            onClick={() => {
              onSelect(index);
            }}
          >
            <StepLabel
              error={step.fields
                .map((field) => !!errors[field] && activeStep > index)
                .reduce((a, b) => a && b)}
            >
              {step.label}
            </StepLabel>
          </Step>
        );
      })}
    </Stepper>
  </ThemeProvider>
);
