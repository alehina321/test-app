import React from "react";
import { Typography, Stack, Avatar } from "@mui/material";

import InsertPhotoOutlinedIcon from "@mui/icons-material/InsertPhotoOutlined";

type ProfileRowType = {
  avatar?: string;
  name: string;
  address: string;
};

export const ProfileRow = ({ avatar, name, address }: ProfileRowType) => (
  <Stack direction="row" alignItems="center" spacing={2}>
    <Avatar src={avatar} variant="rounded">
      <InsertPhotoOutlinedIcon />
    </Avatar>
    <Stack justifyContent="space-between">
      <Typography variant="body100" component="p">
        {name}
      </Typography>
      <Typography variant="caption" sx={{ color: "gray" }}>
        {address}
      </Typography>
    </Stack>
  </Stack>
);
