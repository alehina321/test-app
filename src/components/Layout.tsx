import React from "react";
import { Outlet } from "react-router-dom";
import { SideNav } from "./SideNav";
import { Container } from "@mui/material";

export const Layout = () => {
  return (
    <Container
      maxWidth={false}
      sx={{
        height: "100vh",
        padding: 0,
        display: "flex",
        gap: "20px",
        overflow: "auto",
      }}
    >
      <SideNav />

      <Container
        maxWidth={false}
        sx={{
          width: "100%",
        }}
      >
        <Outlet />
      </Container>
    </Container>
  );
};
