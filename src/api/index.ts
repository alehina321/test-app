import { mockTable } from "../mock-data";
import {
  UseMutationOptions,
  useMutation,
  useQuery,
  UseMutationResult,
  UseQueryOptions,
  UseQueryResult,
  useQueryClient,
} from "@tanstack/react-query";

export type ErrorType = { [key: string]: string };

type QueryOptions<ResponseType, ParamsType = {}> = Omit<
  UseQueryOptions<ResponseType, ErrorType, ResponseType, any[]>,
  "queryKey" | "queryFn"
> &
  ParamsType;

type MutationOptions<ResponseType, ParamsType = {}, Variables = void> = Omit<
  UseMutationOptions<ResponseType, ErrorType, Variables>,
  "mutationFn" | "mutationKey"
> &
  ParamsType;

type QueryHook<ResponseType = any, ParamsType = {}> = {} extends ParamsType
  ? (
      options?: QueryOptions<ResponseType, ParamsType>
    ) => UseQueryResult<ResponseType, ErrorType>
  : (
      options: QueryOptions<ResponseType, ParamsType>
    ) => UseQueryResult<ResponseType, ErrorType>;

type MutationHook<
  ResponseType = any,
  ParamsType = {},
  Variables = void
> = {} extends ParamsType
  ? (
      options?: MutationOptions<ResponseType, ParamsType, Variables>
    ) => UseMutationResult<ResponseType, ErrorType, Variables>
  : (
      options: MutationOptions<ResponseType, ParamsType, Variables>
    ) => UseMutationResult<ResponseType, ErrorType, Variables>;

export const useGetApartments: QueryHook<Apartment[]> = () => {
  return useQuery({
    queryKey: ["apartments"],

    queryFn: () => {
      return mockTable;
    },
  });
};

export const useGetApartment: QueryHook<Apartment, { id: string }> = ({
  id,
}) => {
  return useQuery({
    queryKey: ["apartment", { id }],

    queryFn: () => {
      return mockTable.find((item) => item.id === id);
    },
  });
};

export const useCreateApartment: QueryHook<Apartment[]> = () => {
  return useQuery({
    queryKey: ["new-apartment"],

    queryFn: () => {
      mockTable.push({
        id: mockTable.length.toString(),
        name: "",
        address: "",
        status: "draft",
        booking: 0,
        messages: 0,
      });
      return mockTable;
    },
  });
};

export const useEditApartment: MutationHook<any, { id: string }, Apartment> = ({
  id,
}) => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: ({ name, address, status, booking, messages }) => {
      const promise = new Promise(function (resolve, reject) {
        const index = mockTable.findIndex((elem) => elem.id === id);
        mockTable.splice(index, 1);
        mockTable.push({
          id,
          name,
          address,
          status,
          booking,
          messages,
        });
        resolve(mockTable);
      });

      return promise.then((res) => res);
    },
    mutationKey: ["edit-apartments"],
    onSuccess: () => {
      queryClient.invalidateQueries(["apartments"]);
    },
  });
};
